import React from 'react';
import PropTypes from 'prop-types';
import {Router} from 'react-router';

import {Login, Register, ForgotPassword} from './containers/auth';
import Admin from './containers/admin';

export const paths = {
    ROOT: '/',
    LOGIN: '/login',
    REGISTER: '/register',
    FORGOT_PASSWORD: '/forgot-password',
    ADMIN: '/admin'
};

const Routers = ({history, getState}) => {

    const routes = [
        {
            path: paths.ROOT,
            component: Login
        },
        {
            path: paths.LOGIN,
            component: Login
        },
        {
            path: paths.REGISTER,
            component: Register
        },
        {
            path: paths.FORGOT_PASSWORD,
            component: ForgotPassword
        },
        {
            path: paths.ADMIN,
            component: Admin
        }
    ];

    return <Router history={history} routes={routes}/>
};

Routers.propTypes = {
    history: PropTypes.object.isRequired
};

export default Routers;
