import React from 'react';
import {ApolloProvider} from 'react-apollo';
import {browserHistory} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';
import configureStore from '../core/store';
import client from '../core/apolloClient';
import Router from '../router';

export default  () => {
    const store = configureStore();
    const syncedHistory = syncHistoryWithStore(browserHistory, store);

    return (
        <ApolloProvider store={store} client={client}>
            <Router history={syncedHistory} getState={store.getState} />
        </ApolloProvider>
    );
}