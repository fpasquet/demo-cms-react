import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Form} from 'antd';
import {FormBuilder, Fields} from 'core/helpers/form/index';

class LoginForm extends Component {

    static propTypes = {
        handleSubmit: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.formBuilder = new FormBuilder(props.form);
        this.formBuilder
            .setCallbackSubmit(props.handleSubmit)
            .add('usernameOrEmail', Fields.InputType, {
                placeholder: 'Username or email',
                icon: 'user',
                validators: [{
                    required: true
                }]
            })
            .add('password', Fields.InputType, {
                placeholder: 'Password',
                type: 'password',
                icon: 'lock',
                validators: [{
                    required: true
                }]
            })
            .add('remember', Fields.CheckboxType, {
                label: 'Remember me'
            })
            .add('submit', Fields.ButtonType, {
                label: 'Sign In'
            })
    }

    render() {
        const {children} = this.props;
        return (
            <Form onSubmit={this.formBuilder.handleSubmit}>
                <div className="logo">
                    <img src={require('assets/images/logo-dark.png')} alt="" /> CMS
                </div>
                <p className="title">Login to your account</p>
                {this.formBuilder.renderFields()}
                {children}
            </Form>
        );
    }
}

export default Form.create()(LoginForm);