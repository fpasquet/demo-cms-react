import React, {Component} from 'react';
import ForgotPasswordForm from './form';
import {Link} from 'react-router';
import {paths} from 'router';
import './styles.scss';

export default class ForgotPassword extends Component {

    handleSubmit = (values) => {
        console.log('Received values of form: ', values);
    };

    render() {
        return (
            <div id="forgotPasswordContainer" className="authContainer">
                <ForgotPasswordForm handleSubmit={this.handleSubmit}>
                    <div className="blockBetween">
                        <Link to={paths.LOGIN}>Login</Link>
                        <Link to={paths.REGISTER}>Create account</Link>
                    </div>
                </ForgotPasswordForm>
            </div>
        );
    }
}