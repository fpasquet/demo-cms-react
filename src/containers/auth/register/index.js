import React, {Component} from 'react';
import RegisterForm from './form';
import {Link} from 'react-router';
import {paths} from 'router';
import './styles.scss';

export default class Register extends Component {

    handleSubmit = (values) => {
        console.log('Received values of form: ', values);
    };

    render() {
        return (
            <div id="registerContainer" className="authContainer">
                <RegisterForm handleSubmit={this.handleSubmit}>
                    <p className="centerAndMarginTop">Already have an account ? <Link to={paths.LOGIN}>Login</Link></p>
                </RegisterForm>
            </div>
        );
    }
}