import React from 'react';
import {Button} from 'antd';

const ButtonType = ({options: {label, theme, size}}) => {
    return (
        <Button type={theme ? theme : 'primary'} size={size ? size : 'large'} htmlType="submit">
            {label}
        </Button>
    );
};

export default ButtonType;