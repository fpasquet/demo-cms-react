import React from 'react';
import PropTypes from 'prop-types';
import {pick} from 'lodash';
import {Form, Checkbox} from 'antd';
import {getRulesByOptions} from '../utils';

const CheckboxType = ({name, options}, context) => {
    let optionsAllowed = pick(options, []);

    return (
        <Form.Item>
            {context.form.getFieldDecorator(name, {rules: getRulesByOptions(options)})(
                <Checkbox {...optionsAllowed}>{options.label}</Checkbox>
            )}
        </Form.Item>
    );
};
CheckboxType.propTypes = {
    name: PropTypes.string.isRequired
};
CheckboxType.contextTypes = {
    form: PropTypes.object
};

export default CheckboxType;