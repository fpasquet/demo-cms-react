import {ApolloClient, createNetworkInterface} from 'react-apollo';
import {SubscriptionClient, addGraphQLSubscriptions} from 'subscriptions-transport-ws';

const {
    REACT_APP_GRAPHQL_URL: graphQLUrl,
    REACT_APP_GRAPHQL_URL_SUBSCRIPTIONS: GraphQLUrlSubscriptions
} = process.env;

const wsClient = new SubscriptionClient(GraphQLUrlSubscriptions, {reconnect: false});
const networkInterface = createNetworkInterface({
    uri: graphQLUrl,
    opts: {
        credentials: 'same-origin'
    }
});
networkInterface.use([{
    applyMiddleware(req, next) {
        setTimeout(next, 500);
    }
}]);
const networkInterfaceWithSubscriptions = addGraphQLSubscriptions(
    networkInterface,
    wsClient
);

export default new ApolloClient({
    networkInterface: networkInterfaceWithSubscriptions
});